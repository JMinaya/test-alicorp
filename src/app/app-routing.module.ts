import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorPageComponent } from './shared/components/error-page/error-page.component';
import { BUSINESS_PATHS, SHARED_PATHS } from './shared/parameters/paths';

const routes: Routes = [
  {
    path: SHARED_PATHS.NOT_FOUND,
    component: ErrorPageComponent
  },
  {
    path: BUSINESS_PATHS.USER,
    loadChildren: () => import('./business/business.module').then( m => m.BusinessModule )
  },
  {
    path: '**',
    redirectTo: '404'
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot( routes )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
