export interface UserResponse {
    results: Array<User>;
}

export interface User {
    created?: string;
    episode?: Array<any>;
    gender?: string;
    id?: number;
    image?: string;
    location?: Object
    name?: string;
    origin?: Object
    species?: string;
    status?: string;
    type?: string;
    url?: string;
}
