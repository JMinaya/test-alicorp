import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListadoComponent } from './components/listado/listado.component';
import { MaterialModule } from 'src/app/core/modules/material/material.module';
import { UsersRoutingModule } from './users-routing.module';
import { EliminarComponent } from './components/eliminar/eliminar.component';



@NgModule({
  declarations: [ListadoComponent, EliminarComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    MaterialModule
  ]
})
export class UsersModule { }
