import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListadoComponent } from './components/listado/listado.component';
import { BUSINESS_PATHS } from '../../shared/parameters/paths';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: ListadoComponent
      },
      {
        path: `${BUSINESS_PATHS.USER}/listado`,
        component: ListadoComponent
      }
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild( routes )
  ],
  exports: [
    RouterModule
  ]
})
export class UsersRoutingModule { }
