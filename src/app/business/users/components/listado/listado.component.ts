import { Component, OnInit, ViewChild } from '@angular/core';
import { User, UserResponse } from '../../interfaces/users.interface';
import { UsersService } from '../../services/users.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import { EliminarComponent } from '../eliminar/eliminar.component';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit{

  dataSource: MatTableDataSource<User> = new MatTableDataSource();
  displayedColumns: string[] = ['id', 'name', 'gender', 'action'];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor( 
    private usersService: UsersService,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.usersService.getUsers() 
      .subscribe((resp: UserResponse) => {
        this.dataSource = new MatTableDataSource<User>(resp.results);
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
        }, 1000);
      });
  }

  openModal(id: number) {
    const dialogRef = this.dialog.open(EliminarComponent);
    dialogRef.componentInstance.send.subscribe(() => {
      this.dataSource.data = this.dataSource.data.filter(f => f.id !== id);
      dialogRef.close();
    });
  }
}
