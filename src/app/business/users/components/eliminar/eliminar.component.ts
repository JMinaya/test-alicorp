import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-eliminar',
  templateUrl: './eliminar.component.html',
  styleUrls: ['./eliminar.component.css']
})
export class EliminarComponent implements OnInit {
  @Output() send = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  deleteUser(): void {
    this.send.emit();
  }

}
