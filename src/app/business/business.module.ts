import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../core/modules/material/material.module';
import { UsersRoutingModule } from './users/users-routing.module';
import { UsersModule } from './users/users.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    UsersModule,
    MaterialModule
  ]
})
export class BusinessModule { }
